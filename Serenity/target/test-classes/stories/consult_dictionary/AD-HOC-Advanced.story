Meta:

Narrative:
As a user
I want to log in to my account
So that I can view my username in the top right corner of the landing page
After that I want to open "AD HOC - ADVANCED -> SUPERVALU HIERARCHY - WEEKLY"
So that I can view the report name on the top of the page
After that I fill all wanted filters and execute the report
As a result I see the report results with correct filters and elements present on the report results




Scenario: User can log in to his account
Given I am on Login page.
When Passing successful authentication.
Then I get to the landing page with username in the right corner.
When I open ADHOC Advanced Weekly
Then I see "Report name" on the top of the page
When I fill all wanted filters and click on "Show Report"
Then I see report results and want to save them
Then I want to check that report is saved correctly