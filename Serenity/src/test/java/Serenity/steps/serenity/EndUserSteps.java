package Serenity.steps.serenity;

import Serenity.pages.*;
import net.thucydides.core.annotations.Step;

import static Serenity.pages.LandingPage.*;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    LoginPage LoginPage;
    LandingPage LandingPage;
    Header Header;
    AdHocAdvSHWeekly AdHocAdvSHWeekly;
    MySavedReports MySavedReports;

    @Step
    public void openLoginPage() {
        LoginPage.open();
        //getDriver().navigate().refresh();
        getDriver().manage().window().maximize();
    }
    @Step
    public void fillInUsername() { LoginPage.fillInUserName(); }
    @Step
    public void fillInPassword() {
        LoginPage.fillInPassword();
    }
    @Step
    public void acceptTerms() {
        LoginPage.acceptTerms();
    }
    @Step
    public void pressLoginButton() { LoginPage.pressLoginButton(); }
    @Step
    public void assertOnLandingPage() {
        LandingPage.assertIsOpened();
    }
    @Step
    public void assertLoggedIn() {
        Header.assertLoggedIn();
    }
    @Step
    public void OpenADHocAdvWeekly (){ LandingPage.OpenADHocAdvWeekly();}
    @Step
    public void assertReportNameAD() { AdHocAdvSHWeekly.assertReportNameAD(); }
    @Step
    public void FilterSelections() throws InterruptedException { AdHocAdvSHWeekly.FilterSelections(); }
    @Step
    public void DataAssertions() throws InterruptedException { AdHocAdvSHWeekly.DataAssertions(); }
    @Step
    public void SavingAssertion() throws InterruptedException { MySavedReports.SavingAssertion();}

}