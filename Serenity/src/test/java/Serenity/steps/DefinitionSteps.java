package Serenity.steps;


import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import Serenity.steps.serenity.EndUserSteps;

public class DefinitionSteps {

    @Steps
    EndUserSteps endUser;

    @Given("I am on Login page.")
    public void openLoginPage(){
        endUser.openLoginPage();
    }
    @When("Passing successful authentication.")
    public void login(){
        endUser.fillInUsername();
        endUser.fillInPassword();
        endUser.acceptTerms();
        endUser.pressLoginButton();
    }
    @Then("I get to the landing page with username in the right corner.")
    public void assertLoggedIn(){
        endUser.assertOnLandingPage();
        endUser.assertLoggedIn();
    }
    @When("I open ADHOC Advanced Weekly")
    public void ReportOpening(){
        endUser.OpenADHocAdvWeekly();
    }
    @Then ("I see \"Report name\" on the top of the page")
    public void assertReportName(){
        endUser.assertReportNameAD();
    }
    @When ("I fill all wanted filters and click on \"Show Report\"")
    public void FilterSelections() throws InterruptedException {
        endUser.FilterSelections();
    }
    @Then("I see report results and want to save them")
    public void DataAssertions() throws InterruptedException {
        endUser.DataAssertions();
    }
    @Then("I want to check that report is saved correctly")
    public void SavingAssertion() throws InterruptedException {
        endUser.SavingAssertion();
    }
}
