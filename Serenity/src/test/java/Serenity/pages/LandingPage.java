package Serenity.pages;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://svinsights.icucsolutions.com/ProjectForms/NewsUpdates.aspx?vid=11&mid=379&ShowSplash=true")
public class LandingPage extends PageObject {
    @FindBy (xpath = "html/body/form/div[3]/div[2]/div/div/div[1]/h1")
    private WebElement pageTitle;
    public void assertIsOpened() {
        element(pageTitle).shouldContainOnlyText("LANDING PAGE");
    }
    public void OpenADHocAdvWeekly() {
        openAt("https://svinsights.icucsolutions.com/V0317/Dashboard_AdhocReportSpectraStoreClusters_V3.aspx?vid=28&mid=1734");
    }
}

