package Serenity.pages;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;
import Serenity.pages.*;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;


@DefaultUrl ("https://svinsights.icucsolutions.com/V0917/ManageSavedReports.aspx?vid=6&mid=1217")
public class MySavedReports extends PageObject {
    @FindBy(xpath = "//a[contains(text(),'Saved On')]")
    private WebElement SavedOnSwitcher;
    @FindBy(xpath = "//span[@id='ctl00_MainContent_grdMembers_ctl00_ctl04_lblNewsTitle']")
    private WebElement SavedName;
    @FindBy(xpath = "//span[@id='ctl00_MainContent_grdMembers_ctl00_ctl04_lblReportName']")
    private WebElement SavedReportName;
    @FindBy(xpath = "//span[@id='ctl00_MainContent_grdMembers_ctl00_ctl04_LblEmail']")
    private WebElement EmailID;
    @FindBy(xpath = "//td[contains(text(),'Last 10 Week(s)')]")
    private WebElement DateSelection;
    @FindBy(xpath = "//span[@id='ctl00_MainContent_grdMembers_ctl00_ctl04_lblPostedBy']")
    private WebElement SavedDesciption;

    public void SavingAssertion() throws InterruptedException {
    element(SavedOnSwitcher).click();
    TimeUnit.SECONDS.sleep(10);
    element(SavedOnSwitcher).click();
    TimeUnit.SECONDS.sleep(10);
    element(SavedName).isPresent();
    element(SavedDesciption).isPresent();
    element(DateSelection).shouldContainOnlyText("LAST 10 WEEK(S)");
    element(EmailID).shouldContainOnlyText("SERGEY.KLIKICH@ICONTROLDATA.NET");





    }
}
