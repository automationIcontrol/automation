package Serenity.pages;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://svinsights.icucsolutions.com/Login.aspx")
public class LoginPage extends PageObject {
    @FindBy (name="txtusname")
    WebElementFacade fieldUsername;
    @FindBy (name="txtpass")
    WebElementFacade fieldPassword;
    @FindBy (name="chkAccept")
    WebElementFacade Terms;
    @FindBy (name="ImageButton1")
    WebElementFacade LoginButton;
    public void fillInUserName() {
        element(fieldUsername).type("svuqacorporate");
    }

    public void fillInPassword() {
        element(fieldPassword).type("svuq!@#");
    }

    public void acceptTerms() { element(Terms).click(); }

    public void pressLoginButton() { element(LoginButton).click(); }
}
