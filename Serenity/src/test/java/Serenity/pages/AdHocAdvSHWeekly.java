package Serenity.pages;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.AcceptAlert;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@DefaultUrl ("https://svinsights.icucsolutions.com/V0317/Dashboard_AdhocReportSpectraStoreClusters_V3.aspx?vid=28&mid=1734")
public class AdHocAdvSHWeekly extends PageObject {
    @FindBy(xpath = "html/body/form/div[3]/div[2]/div/div[3]/h1")
    private WebElement ReportName;
    @FindBy(id = "ctl00_MainContent_ddlWeekView_Arrow")
    private WebElement WeekViewSelector;
    @FindBy(xpath = "html/body/form/div[1]/div/div/ul/li[2]")
    private WebElement ADWeek;
    @FindBy(name = "ctl00$MainContent$dtFromDate$txtCalendar")
    private WebElement FromDate;
    @FindBy(name = "ctl00$MainContent$dtToDate$txtCalendar")
    private WebElement ToDate;
    @FindBy(id = "ctl00_MainContent_ddlBanners_Arrow")
    private WebElement BannerSelector;
    //@FindBy(xpath = "//div[@id='ctl00_MainContent_ddlBanners_DropDown']//li[contains(@class,'rcbHovered')]//label")
    @FindBy(xpath = "//li[contains(@class,'rcbHovered')]//label")
    private WebElement CUB;
    @FindBy(id = "ctl00_MainContent_btnApplyBanner_input")
    private WebElement BannerApply;
    @FindBy(id = "ctl00_MainContent_ddlSubBanners_Arrow")
    private WebElement SubBannerSelector;
    @FindBy(xpath = "html/body/form/div[1]/div/div/ul/li[4]/label/input")
    private WebElement DeselectionOWNEDLLCS;
    @FindBy(id = "ctl00_MainContent_btnapplysubbanner_input")
    private WebElement SubBannerApply;
    @FindBy(id = "ctl00_MainContent_ddlDepartments_Arrow")
    private WebElement DepartmentsSelector;
    @FindBy(xpath = "html/body/form/div[1]/div/div/ul/li[19]/label/input")
    private WebElement TotalAlcohol;
    @FindBy(id = "ctl00_MainContent_btnApplyDept_input")
    private WebElement DepartmentApply;
    @FindBy(id = "ctl00_MainContent_ddlCategories_Arrow")
    private WebElement CategorySelector;
    @FindBy(xpath = "html/body/form/div[1]/div/div/ul/li[1]/label/input")
    private WebElement AbovePremiumBeer;
    @FindBy(id = "ctl00_MainContent_btnApplyCategory_input")
    private WebElement CategoryApply;
    @FindBy(id = "ctl00_MainContent_ddlbrandtype_Arrow")
    private WebElement BrandTypeSelector;
    @FindBy(xpath = "html/body/form/div[1]/div/div/ul/li[3]")
    private WebElement NationalBrand;
    @FindBy(id = "ctl00_MainContent_btnapplybrandtype_input")
    private WebElement BrandTypeApply;
    @FindBy(id = "ctl00_MainContent_btnShowReport")
    private WebElement ShowReport;
    @FindBy(id = "ctl00_MainContent_BtnsaveRep")
    private WebElement SaveButton;
    @FindBy(id = "ctl00_MainContent_Btnsharerep")
    private WebElement ShareButton;
    @FindBy(id = "ctl00_MainContent_btnExport")
    private WebElement ExportButton;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[15]/div/div[1]/div[12]/div/div/div/span/div[1]/span")
    private WebElement NameInReportHeader;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[15]/div/div[1]/div[12]/div/div/div/span/div[2]/span")
    private WebElement FiltersInReportHeader;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[5]/div/div/div/span/div[1]/span[2]")
    private WebElement Instructions;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[3]/div/div/img")
    private WebElement Logo;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[13]/div/div[1]/div[12]/div/div/div/span/div/span")
    private WebElement UPCLookup;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[12]/div/div/div/span/div/span[3]")
    private WebElement DefaultAnalysisPath;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[6]/div/div[1]/div[8]/div/canvas")
    private WebElement MetricsBar;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[6]/div/div[1]/div[1]/canvas")
    private WebElement ADlvl1;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[6]/div/div[1]/div[5]/div/canvas")
    private WebElement ADlvl1DataTable;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[6]/div/div[1]/div[11]/div[1]/div[2]/canvas[2]")
    private WebElement MetricsDataTable;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[7]/div/div/div/div[1]/div/span/div/span")
    private WebElement AnalyzeByLabel;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[8]/div/div/div/div[1]/div/span/div/span")
    private WebElement AnalyzeByLabel2;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[9]/div/div/div/div[1]/div/span/div/span")
    private WebElement AnalyzeByLabel3;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[10]/div/div/div/div[1]/div/span/div/span")
    private WebElement AnalyzeByLabel4;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[11]/div/div/div/div[1]/div/span/div/span")
    private WebElement AnalyzeByLabel5;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[18]/div/div/div/div[1]/div/span/div/span")
    private WebElement SalesByLabel;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[18]/div/div/div/div[2]/span/div[2]/span")
    private WebElement SalesByDropDown;
    @FindBy(xpath = "html/body/div[5]/div/div[1]/div/span")
    private WebElement SalesByDropDown_Sales;
    @FindBy(xpath = "html/body/div[5]/div/div[2]/div/span")
    private WebElement SalesByDropDown_IDSales;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[7]/div/div/div/div[2]/span/div[2]/span")
    private WebElement AnalyzeByDropDown;
    @FindBy(xpath = "html/body/div[5]/div/div[1]/div/span")
    private WebElement AnalyzeByBanner;
    @FindBy(xpath = "html/body/div[5]/div/div[2]/div/span")
    private WebElement AnalyzeBySubBanner;
    @FindBy(xpath = "html/body/div[5]/div/div[3]/div/span")
    private WebElement AnalyzeByStoreCluster;
    @FindBy(xpath = "html/body/div[5]/div/div[4]/div/span")
    private WebElement AnalyzeByDepartment;
    @FindBy(xpath = "html/body/div[5]/div/div[5]/div/span")
    private WebElement AnalyzeBySubDepartment;
    @FindBy(xpath = "html/body/div[5]/div/div[6]/div/span")
    private WebElement AnalyzeByCategory;
    @FindBy(xpath = "html/body/div[5]/div/div[7]/div/span")
    private WebElement AnalyzeBySubCategory;
    @FindBy(xpath = "html/body/div[5]/div/div[8]/div/span")
    private WebElement AnalyzeBySegment;
    @FindBy(xpath = "html/body/div[5]/div/div[9]/div/span")
    private WebElement AnalyzeByBrand;
    @FindBy(xpath = "html/body/div[5]/div/div[10]/div/span")
    private WebElement AnalyzeByProductSizeDesc;
    @FindBy(xpath = "html/body/div[5]/div/div[11]/div/span")
    private WebElement AnalyzeByProductFullDescription;
    @FindBy(xpath = "html/body/div[5]/div/div[12]/div/span")
    private WebElement AnalyzeByPrivateBrand;
    @FindBy(xpath = "html/body/div[5]/div/div[13]/div/span")
    private WebElement AnalyzeByUPC;
    @FindBy(xpath = "html/body/div[5]/div/div[14]/div/span")
    private WebElement AnalyzeByRegion;
    @FindBy(xpath = "html/body/div[5]/div/div[15]/div/span")
    private WebElement AnalyzeByState;
    @FindBy(xpath = "html/body/div[5]/div/div[16]/div/span")
    private WebElement AnalyzeByCity;
    @FindBy(xpath = "html/body/div[5]/div/div[17]/div/span")
    private WebElement AnalyzeByStore;
    @FindBy(xpath = "html/body/div[5]/div/div[18]/div/span")
    private WebElement AnalyzeByFiscalYear;
    @FindBy(xpath = "html/body/div[5]/div/div[19]/div/span")
    private WebElement AnalyzeByFiscalQuarter;
    @FindBy(xpath = "html/body/div[5]/div/div[20]/div/span")
    private WebElement AnalyzeByFiscalPeriod;
    @FindBy(xpath = "html/body/div[5]/div/div[21]/div/span")
    private WebElement AnalyzeByFiscalWeek;
    @FindBy(xpath = "html/body/div[5]/div/div[22]/div/span")
    private WebElement AnalyzeByWeekStartDate;
    @FindBy(xpath = "html/body/div[2]/div[4]/div[2]/div/div/div[12]/div/div/div/span/div")
    private WebElement DefaultAnalyzeBy;
    @FindBy(id = "ctl00_MainContent_txtReportName")
    private WebElement txtReportName;
    @FindBy(id = "ctl00_MainContent_txtReportDescr")
    private WebElement txtReportDescr;
    @FindBy(id = "ctl00_MainContent_RadDates_1")
    private WebElement RelativeDateSelector;
    @FindBy(id = "ctl00_MainContent_DrpDateVal")
    private WebElement RelativeWeekSelector;
    @FindBy(xpath = "//option[@value='10']")
    private WebElement RelativeWeek10;
    @FindBy(id = "ctl00_MainContent_rdSchedule_0")
    private WebElement SubscribeYes;
    @FindBy(id = "ctl00_MainContent_txtemail")
    private WebElement EmailField;
    @FindBy(id = "ctl00_MainContent_btnsave")
    private WebElement SaveBtnOnPopUp;

    public AdHocAdvSHWeekly() {
    }


    public void assertReportNameAD(){
        element(ReportName).shouldContainOnlyText("AD HOC - ADVANCED -> SUPERVALU HIERARCHY - WEEKLY");
    }

    public void FilterSelections() throws InterruptedException {
        element(WeekViewSelector).click();
        element(ADWeek).click();
        element(FromDate).type("10/29/2017");
        element(ToDate).type("11/11/2017");
        element(BannerSelector).click();
        element(CUB).click();
        TimeUnit.SECONDS.sleep(4);
        element(BannerSelector).click();
        element(BannerApply).click();
        TimeUnit.SECONDS.sleep(10);
        element(SubBannerSelector).click();
        element(DeselectionOWNEDLLCS).click();
        element(SubBannerSelector).click();
        element(SubBannerApply).click();
        TimeUnit.SECONDS.sleep(10);
        element(DepartmentsSelector).click();
        element(TotalAlcohol).click();
        element(DepartmentsSelector).click();
        element(DepartmentApply).click();
        TimeUnit.SECONDS.sleep(10);
        element(CategorySelector).click();
        element(AbovePremiumBeer).click();
        element(CategorySelector).click();
        element(CategoryApply);
        TimeUnit.SECONDS.sleep(15);
        element(BrandTypeSelector).click();
        element(NationalBrand).click();
        element(BrandTypeSelector).click();
        element(BrandTypeApply).click();
        TimeUnit.SECONDS.sleep(15);
        element(ShowReport).click();
        TimeUnit.SECONDS.sleep(120);



    }

    public void DataAssertions() throws InterruptedException {
        element(SaveButton).isPresent();
        element(ShareButton).isPresent();
        element(ExportButton).isPresent();
        getDriver().switchTo().frame("Iframechart");
        element(NameInReportHeader).shouldContainOnlyText("Ad Hoc - Advanced - Weekly");
        element(FiltersInReportHeader).shouldContainOnlyText("Banner: CUB; Sub-Banner: CUB DIAMOND LAKE LLC,CUB FOODS,CUB FRANCHISE; Category: ALL OTHER BEER, ALL OTHER WINE, BEER COUPONS and 21 more\n" +
                "Sales by: Sales\n" +
                "From: 10/29/2017 To: 11/11/2017(AD Week)");
        element(Instructions).shouldContainOnlyText("Instructions");
        element(Logo).isPresent();
        element(UPCLookup).shouldContainOnlyText("UPC Lookup");
        element(DefaultAnalysisPath).shouldContainOnlyText("Store Cluster --> Store  --> Category  --> Segment  --> Brand");
        element(MetricsBar).isPresent();
        element(ADlvl1).isPresent();
        element(ADlvl1DataTable).isPresent();
        element(MetricsDataTable).isPresent();
        element(AnalyzeByLabel).isPresent();
        element(AnalyzeByLabel2).isPresent();
        element(AnalyzeByLabel3).isPresent();
        element(AnalyzeByLabel4).isPresent();
        element(AnalyzeByLabel5).isPresent();
        element(SalesByLabel).isPresent();
        element(SalesByDropDown).click();
        element(SalesByDropDown_Sales).isPresent();
        element(SalesByDropDown_IDSales).isPresent();
        element(SalesByDropDown_Sales).shouldContainOnlyText("Sales");
        element(SalesByDropDown_IDSales).shouldContainOnlyText("ID Sales");
        element(SalesByDropDown_IDSales).click();
        TimeUnit.SECONDS.sleep(10);
        element(FiltersInReportHeader).shouldContainOnlyText("Banner: CUB; Sub-Banner: CUB DIAMOND LAKE LLC,CUB FOODS,CUB FRANCHISE; Category: ALL OTHER BEER, ALL OTHER WINE, BEER COUPONS and 21 more\n" +
                "Sales by: ID Sales\n" +
                "From: 10/29/2017 To: 11/11/2017(AD Week)");
        element(DefaultAnalyzeBy).shouldContainOnlyText("Your Analysis Path: Store Cluster --> Store  --> Category  --> Segment  --> Brand");
        element(AnalyzeByDropDown).click();
        element(AnalyzeByBanner).shouldContainOnlyText("Banner");
        element(AnalyzeBySubBanner).shouldContainOnlyText("Sub Banner");
        element(AnalyzeByStoreCluster).shouldContainOnlyText("Store Cluster");
        element(AnalyzeByDepartment).shouldContainOnlyText("Department");
        element(AnalyzeBySubDepartment).shouldContainOnlyText("Sub Department");
        element(AnalyzeByCategory).shouldContainOnlyText("Category");
        element(AnalyzeBySubCategory).shouldContainOnlyText("Sub Category");
        element(AnalyzeBySegment).shouldContainOnlyText("Segment");
        element(AnalyzeByBrand).shouldContainOnlyText("Brand");
        element(AnalyzeByProductSizeDesc).shouldContainOnlyText("Product Size Desc");
        element(AnalyzeByProductFullDescription).shouldContainOnlyText("Product Full Description");
        element(AnalyzeByPrivateBrand).shouldContainOnlyText("Private Brand");
        element(AnalyzeByUPC).shouldContainOnlyText("UPC");
        element(AnalyzeByRegion).shouldContainOnlyText("Region");
        element(AnalyzeByState).shouldContainOnlyText("State");
        element(AnalyzeByCity).shouldContainOnlyText("City");
        element(AnalyzeByStore).shouldContainOnlyText("Store");
        element(AnalyzeByFiscalYear).shouldContainOnlyText("Fiscal Year");
        element(AnalyzeByFiscalQuarter).shouldContainOnlyText("Fiscal Quarter");
        element(AnalyzeByFiscalPeriod).shouldContainOnlyText("Fiscal Period");
        element(AnalyzeByFiscalWeek).shouldContainOnlyText("Fiscal Week");
        element(AnalyzeByWeekStartDate).shouldContainOnlyText("Week Start Date");
        element(AnalyzeByWeekStartDate).click();
        TimeUnit.SECONDS.sleep(10);
        getDriver().switchTo().defaultContent();
        element(SaveButton).click();
        TimeUnit.SECONDS.sleep(7);
        element(txtReportName).isPresent();
        element(txtReportDescr).isPresent();
        String RepName = ("Automation" + Math.floor(Math.random()*15000));
        element(txtReportName).type(RepName);
        String RepDesc = ("Automation_Description"+ Math.floor(Math.random()*15000));
        element(txtReportDescr).type(RepDesc);
        element(RelativeDateSelector).click();
        TimeUnit.SECONDS.sleep(5);
        element(RelativeWeekSelector).click();
        element(RelativeWeek10).click();
        element(SubscribeYes).click();
        TimeUnit.SECONDS.sleep(5);
        element(EmailField).type("sergey.klikich@icontroldata.net");
        element(SaveBtnOnPopUp).click();
        TimeUnit.SECONDS.sleep(5);
        getAlert().getText();
        getAlert().accept();
        openAt("https://svinsights.icucsolutions.com/V0917/ManageSavedReports.aspx?vid=6&mid=1217");












    }
}
