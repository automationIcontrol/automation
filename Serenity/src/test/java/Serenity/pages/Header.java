package Serenity.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://svinsights.icucsolutions.com/ProjectForms/NewsUpdates.aspx?vid=11&mid=379&ShowSplash=true")
public class Header extends PageObject {
    @FindBy(xpath = "html/body/form/div[3]/div[1]/div[2]/ul/li/span")
    private WebElement UserDetails;
    public void assertLoggedIn() {
        element(UserDetails).shouldContainOnlyText("Welcome SVU QA Corporate (Supervalu)");
    }
}
