# README #

In software testing, test automation is the use of special software (separate from the software being tested) to control 
the execution of tests and the comparison of actual outcomes with predicted outcomes. Test automation can automate some 
repetitive but necessary tasks in a formalized testing process already in place, or perform additional testing that would 
be difficult to do manually. Test automation is critical for continuous delivery and continuous testing.

### What is this repository for? ###

This repository created for holding the automation test asserts for Selenium IDE and also exported in JUnit 4.

### How do I get set up? ###

All you need is to open "Source -> Selenium_TestAssets -> JavaDoc" and familiarize with guides and diagrams located there.

## Fast launch guide-line ##
1. Execute Testrun.bat from C:\Users\sergey.klikich\selenium\Testrun.bat
2. Wait until firefox and selenium IDE opens
3. Click on "Favorites" on the "Menu bar"
4. Choose necessery test-suite
5. Click on "Execute test-suite" //Triangle with 3 green horizontal bars
6. Wait for test-suite execution
7. Export the results from "FIle" -> "Export test-suite results"

### Hints ###
1. Make sure that you will run all tests when portal is not slow, it may fail because of timeout.
2. Make sure you delete the saved/subscribed/shared reports after auto-tests execution.

### Who do I talk to? ###

sergey.klikich@icontroldata.net - Test - creator
gaurav.ahuja@icontroldata.net - QA Manager