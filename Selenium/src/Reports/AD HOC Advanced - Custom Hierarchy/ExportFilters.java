package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ExportFilters {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testExportFilters() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'Custom Hierarchy')])[34]")).click();
    assertEquals("(Sunday --> Saturday)", driver.findElement(By.cssSelector("label > span")).getText());
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).sendKeys("10/01/2017");
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).sendKeys("10/07/2017");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.xpath("//li[5]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.xpath("//li[6]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    driver.findElement(By.xpath("//li[4]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("6 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("6 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("CUB DIAMOND LAKE LLC");
    driver.findElement(By.id("ctl00_MainContent_btnapplysubbanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Arrow")).click();
    driver.findElement(By.xpath("//li[112]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Input")).sendKeys("COTTAGE CHEESE_KEMPS_ESSENTIAL_EVERYDAY_FRIENDSHIP_UpdatedProducts_20170802");
    driver.findElement(By.id("ctl00_MainContent_btnApplyHierarchy_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Arrow")).click();
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).sendKeys("2 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).sendKeys("2 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).sendKeys("ESSENTIAL EVERYDAY");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBrand_input")).click();
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    driver.findElement(By.id("ctl00_MainContent_btnExport")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | popup_window | 30000]]
    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | name=popup_window | ]]
    assertEquals("10/01/2017", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[4]")).getText());
    assertEquals("10/07/2017", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[6]")).getText());
    assertEquals("CUB, FARM FRESH, SHOP-N-SAVE, SHOPPERS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[8]")).getText());
    assertEquals("Banner(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[7]")).getText());
    assertEquals("CUB DIAMOND LAKE LLC, CUB FOODS, CUB FRANCHISE, FARM", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[10]")).getText());
    assertEquals("FRESH, SHOPPERS, SNS ST LOUIS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[11]")).getText());
    assertEquals("Sub Banner(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[9]")).getText());
    assertEquals("Hierarchy Name(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[12]")).getText());
    assertEquals("COTTAGE", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[13]")).getText());
    assertEquals("CHEESE_KEMPS_ESSENTIAL_EVERYDAY_FRIENDSHIP_UpdatedProduc", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[14]")).getText());
    assertEquals("ts_20170802", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[15]")).getText());
    assertEquals("Custom Level 1:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[16]")).getText());
    assertEquals("All", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[17]")).getText());
    assertEquals("Custom Level 2:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[18]")).getText());
    assertEquals("All", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[19]")).getText());
    assertEquals("Custom Level 3:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[20]")).getText());
    assertEquals("All", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[21]")).getText());
    assertEquals("Brand(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[22]")).getText());
    assertEquals("ESSENTIAL EVERYDAY, FRIENDSHIP", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[23]")).getText());
    assertEquals("Product Size Description:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[24]")).getText());
    assertEquals("All", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[25]")).getText());
    assertEquals("Ad Hoc - Advanced - Custom", driver.findElement(By.cssSelector("div.textLayer > div")).getText());
    assertEquals("Hierarchy", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[2]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
