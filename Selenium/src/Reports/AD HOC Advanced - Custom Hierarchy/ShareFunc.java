package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ShareFunc {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testShareFunc() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'Custom Hierarchy')])[34]")).click();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).sendKeys("10/01/2017");
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).sendKeys("10/07/2017");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.xpath("//li[5]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.xpath("//li[6]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    driver.findElement(By.xpath("//li[4]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("6 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("6 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("CUB DIAMOND LAKE LLC");
    driver.findElement(By.id("ctl00_MainContent_btnapplysubbanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Arrow")).click();
    driver.findElement(By.xpath("//li[112]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Input")).sendKeys("COTTAGE CHEESE_KEMPS_ESSENTIAL_EVERYDAY_FRIENDSHIP_UpdatedProducts_20170802");
    driver.findElement(By.id("ctl00_MainContent_btnApplyHierarchy_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Arrow")).click();
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).sendKeys("2 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).sendKeys("2 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBrands_Input")).sendKeys("ESSENTIAL EVERYDAY");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBrand_input")).click();
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    driver.findElement(By.id("ctl00_MainContent_Btnsharerep")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [getEval |  | ]]
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_txtSharedReportName")).clear();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_txtSharedReportName")).sendKeys(Case1);
    // ERROR: Caught exception [ERROR: Unsupported command [getEval |  | ]]
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_txtMessage")).clear();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_txtMessage")).sendKeys(Case2);
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_rdShareHierarchy_0")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | null | ]]
    driver.findElement(By.xpath("//div[@id='ctl00_MainContent_pnlShare']/table/tbody/tr[2]/td[2]")).click();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Arrow")).click();
    driver.findElement(By.xpath("//li[16]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Input")).sendKeys("Corporate QA8 (svuqacorporate_8)");
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Input")).sendKeys("Corporate QA8 (svuqacorporate_8)");
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_RdShareUsers_Input")).sendKeys("Corporate QA8 (svuqacorporate_8)");
    driver.findElement(By.id("ctl00_MainContent_ShareHierarchyID_btnsaveShare")).click();
    assertEquals("Report has been shared successfully", closeAlertAndGetItsText());
    driver.findElement(By.id("ctl00_imgLogout")).click();
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("Svuqacorporate_8");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("Svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.linkText("Maintain Reports")).click();
    driver.findElement(By.linkText("Saved On")).click();
    driver.findElement(By.linkText("Saved On")).click();
    assertEquals(Case1, driver.findElement(By.id("ctl00_MainContent_grdMembers_ctl00_ctl04_lblNewsTitle")).getText());
    assertEquals("Ad Hoc - Advanced - Custom Hierarchy", driver.findElement(By.id("ctl00_MainContent_grdMembers_ctl00_ctl04_lblReportName")).getText());
    driver.findElement(By.id("ctl00_MainContent_grdMembers_ctl00_ctl04_BtnView")).click();
    assertEquals("Message from  TestUser Corporate (svuqacorporate):", driver.findElement(By.cssSelector("div.ShareHeader")).getText());
    assertEquals(Case2, driver.findElement(By.id("divInnerMessage")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
