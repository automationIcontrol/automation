package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ExportFiltersTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testExportFilters() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqafiltered");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'SUPERVALU Hierarchy - Weekly')])[12]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlWeekView_Arrow")).click();
    driver.findElement(By.xpath("//li[2]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlWeekView_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlWeekView_Input")).sendKeys("AD Week");
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_imgCalendar")).click();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).click();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).sendKeys("07/16/2017");
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).sendKeys("07/29/2017");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    driver.findElement(By.xpath("//label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.xpath("//li[4]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("CUB DIAMOND LAKE LLC");
    driver.findElement(By.id("ctl00_MainContent_btnapplysubbanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY, DAIRY");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY, DAIRY");
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("3 items checked");
    driver.findElement(By.xpath("//li[4]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY");
    driver.findElement(By.id("ctl00_MainContent_btnApplyDept_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("43 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("43 items checked");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("42 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("42 items checked");
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("41 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("41 items checked");
    driver.findElement(By.xpath("//li[44]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("40 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("40 items checked");
    driver.findElement(By.xpath("//li[43]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("39 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("39 items checked");
    driver.findElement(By.xpath("//li[42]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("38 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("38 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("ALTERNATIVE SALTY SNACKS");
    driver.findElement(By.id("ctl00_MainContent_btnApplyCategory_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).sendKeys("ALL OTHER");
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Arrow")).click();
    driver.findElement(By.xpath("//li[3]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Input")).sendKeys("National Brand");
    driver.findElement(By.id("ctl00_MainContent_btnapplybrandtype_input")).click();
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    driver.findElement(By.id("ctl00_MainContent_btnExport")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | popup_window | 30000]]
    // ERROR: Caught exception [ERROR: Unsupported command [selectPopUp |  | ]]
    assertEquals("CUB, FARM FRESH, HORNBACHERS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[12]")).getText());
    assertEquals("AD Week", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[4]")).getText());
    assertEquals("Ad Hoc Advanced - SUPERVALU", driver.findElement(By.cssSelector("div.textLayer > div")).getText());
    assertEquals("Hierarchy - Weekly", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[2]")).getText());
    assertEquals("National Brand", driver.findElement(By.xpath("//div[34]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
