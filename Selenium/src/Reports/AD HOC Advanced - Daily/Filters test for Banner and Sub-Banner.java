package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class FiltersTestForBannerAndSubBanner {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testFiltersTestForBannerAndSubBanner() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'SUPERVALU Hierarchy - Daily')])[5]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    assertEquals("CUB", driver.findElement(By.cssSelector("label")).getText());
    assertEquals("FARM FRESH", driver.findElement(By.xpath("//li[2]/label")).getText());
    assertEquals("HORNBACHERS", driver.findElement(By.xpath("//li[3]/label")).getText());
    assertEquals("Independent", driver.findElement(By.xpath("//li[4]/label")).getText());
    assertEquals("SHOP-N-SAVE", driver.findElement(By.xpath("//li[5]/label")).getText());
    assertEquals("SHOPPERS", driver.findElement(By.xpath("//li[6]/label")).getText());
    assertEquals("SNS EAST", driver.findElement(By.xpath("//li[7]/label")).getText());
    driver.findElement(By.cssSelector("input.rcbCheckBox")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.xpath("//li[2]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.xpath("//li[3]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.xpath("//li[4]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.xpath("//li[5]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.xpath("//li[6]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("6 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("6 items checked");
    driver.findElement(By.xpath("//li[7]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("All");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("All");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    assertEquals("CUB DIAMOND LAKE LLC", driver.findElement(By.cssSelector("label")).getText());
    assertEquals("CUB FOODS", driver.findElement(By.xpath("//li[2]/label")).getText());
    assertEquals("CUB FRANCHISE", driver.findElement(By.xpath("//li[3]/label")).getText());
    assertEquals("CUB MINORITY OWNED LLCS", driver.findElement(By.xpath("//li[4]/label")).getText());
    assertEquals("FARM FRESH", driver.findElement(By.xpath("//li[5]/label")).getText());
    assertEquals("HORNBACHERS", driver.findElement(By.xpath("//li[6]/label")).getText());
    assertEquals("IGA - Billings (26)", driver.findElement(By.xpath("//li[7]/label")).getText());
    assertEquals("NEW MARKET - FORT WAYNE (6)", driver.findElement(By.xpath("//li[8]/label")).getText());
    assertEquals("SHOPPERS", driver.findElement(By.xpath("//li[9]/label")).getText());
    assertEquals("SNS EAST", driver.findElement(By.xpath("//li[10]/label")).getText());
    assertEquals("SNS ST LOUIS", driver.findElement(By.xpath("//li[11]/label")).getText());
    assertEquals("SNS-Pittsburgh (50)", driver.findElement(By.xpath("//li[12]/label")).getText());
    assertEquals("NON MERCH SALES", driver.findElement(By.xpath("//li[13]/label")).getText());
    assertEquals("NON MERCH SALES CBS", driver.findElement(By.xpath("//li[14]/label")).getText());
    assertEquals("PHARMACY", driver.findElement(By.xpath("//li[15]/label")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
