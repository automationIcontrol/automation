package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class IframeHeader {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testIframeHeader() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | null | ]]
    driver.findElement(By.xpath("(//a[contains(text(),'Custom Hierarchy & Custom Store')])[2]")).click();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).sendKeys("10/08/2017");
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).clear();
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).sendKeys("10/14/2017");
    driver.findElement(By.id("ctl00_MainContent_ddlCustomStoreGroup_Arrow")).click();
    driver.findElement(By.xpath("//li[7]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomStoreGroup_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomStoreGroup_Input")).sendKeys("All Stores Excluding Independent_20170223");
    driver.findElement(By.id("ctl00_MainContent_ddlCustomStoreGroup_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomStoreGroup_Input")).sendKeys("All Stores Excluding Independent_20170223");
    driver.findElement(By.id("ctl00_MainContent_ddlCustomStoreGroup_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomStoreGroup_Input")).sendKeys("All Stores Excluding Independent_20170223");
    driver.findElement(By.id("ctl00_MainContent_btnApplyCustomStoreGroup_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddladhoclevel1_Arrow")).click();
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddladhoclevel1_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddladhoclevel1_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddladhoclevel1_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddladhoclevel1_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddladhoclevel1_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddladhoclevel1_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyAdhoclevel1_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Arrow")).click();
    driver.findElement(By.xpath("//li[14]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCustomHierarchy_Input")).sendKeys("Acosta  plus  SV Mega Hierarchy_20161213");
    driver.findElement(By.id("ctl00_MainContent_btnApplyHierarchy_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlcustomlevel1_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlcustomlevel1_Input")).sendKeys("3 PPR CHLI HUMMUS");
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    assertEquals("Ad Hoc - Advanced - Custom Store & Product Hierarch​y", driver.findElement(By.xpath("/html/body/div[2]/div[4]/div[2]/div/div/div[9]/div/div[1]/div[12]/div/div/div/span/div[1]/span[1]")).getText());
    assertEquals("All Stores Excluding Independent_20170223\nHierarchy Name: Acosta plus SV Mega Hierarchy_20161213\nSales by: Sales\nFrom: 10/8/2017 To: 10/14/2017", driver.findElement(By.xpath("/html/body/div[2]/div[4]/div[2]/div/div/div[9]/div/div[1]/div[12]/div/div/div/span/div[2]/span[3]")).getText());
    assertEquals("Custom Store Name:", driver.findElement(By.xpath("/html/body/div[2]/div[4]/div[2]/div/div/div[9]/div/div[1]/div[12]/div/div/div/span/div[2]/span[1]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
