package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TimeFrameTestPeriod {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testTimeFrameTestPeriod() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.linkText("SUPERVALU Hierarchy")).click();
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).clear();
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).sendKeys("325266");
    driver.findElement(By.id("ctl00_MainContent_VendorApply_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    driver.findElement(By.xpath("//li[4]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("CUB DIAMOND LAKE LLC");
    driver.findElement(By.id("ctl00_MainContent_btnapplysubbanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Arrow")).click();
    driver.findElement(By.xpath("//li[6]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("ALTERNATIVE SALTY SNACKS");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("ALTERNATIVE SALTY SNACKS");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("ALTERNATIVE SALTY SNACKS");
    driver.findElement(By.id("ctl00_MainContent_btnApplyCategory_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Arrow")).click();
    driver.findElement(By.xpath("//li[3]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Input")).sendKeys("National Brand");
    driver.findElement(By.id("ctl00_MainContent_btnapplybrandtype_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSalesBy_Arrow")).click();
    driver.findElement(By.cssSelector("li.rcbHovered.")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSalesBy_Arrow")).click();
    driver.findElement(By.xpath("//li[2]")).click();
    driver.findElement(By.id("ctl00_MainContent_btnapplySalesBy_input")).click();
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    driver.findElement(By.id("ctl00_MainContent_btnsaveRepstatus")).click();
    assertEquals("Recently Closed Period (FY", driver.findElement(By.xpath("/html/body/div[2]/div[4]/div[2]/div/div/div[20]/div/div[1]/div[12]/div/div/div/span/div/span[1]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
