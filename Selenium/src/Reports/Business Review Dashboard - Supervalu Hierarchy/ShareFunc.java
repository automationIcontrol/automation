package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ShareFunc {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testShareFunc() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.linkText("SUPERVALU Hierarchy")).click();
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).clear();
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).sendKeys("325266");
    driver.findElement(By.id("ctl00_MainContent_VendorApply_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    driver.findElement(By.xpath("//li[4]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("CUB DIAMOND LAKE LLC");
    driver.findElement(By.id("ctl00_MainContent_btnapplysubbanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Arrow")).click();
    driver.findElement(By.cssSelector("input.rcbCheckBox")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("");
    driver.findElement(By.xpath("//li[2]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("AUTHENTIC HISPANIC");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("AUTHENTIC HISPANIC");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("AUTHENTIC HISPANIC");
    driver.findElement(By.id("ctl00_MainContent_btnApplyCategory_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).sendKeys("WAREHOUSE SNACKS");
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).sendKeys("WAREHOUSE SNACKS");
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).sendKeys("WAREHOUSE SNACKS");
    driver.findElement(By.id("ctl00_MainContent_btnApplySubCategory_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSalesBy_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSalesBy_Arrow")).click();
    driver.findElement(By.xpath("//li[2]")).click();
    driver.findElement(By.id("ctl00_MainContent_btnapplySalesBy_input")).click();
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    driver.findElement(By.id("ctl00_MainContent_btnsaveRepstatus")).click();
    driver.findElement(By.id("ctl00_MainContent_Btnsharerep")).click();
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).sendKeys("");
    // ERROR: Caught exception [ERROR: Unsupported command [getEval |  | ]]
    driver.findElement(By.id("ctl00_MainContent_txtSharedReportName")).clear();
    driver.findElement(By.id("ctl00_MainContent_txtSharedReportName")).sendKeys(Case1);
    // ERROR: Caught exception [ERROR: Unsupported command [getEval |  | ]]
    driver.findElement(By.id("ctl00_MainContent_txtMessage")).clear();
    driver.findElement(By.id("ctl00_MainContent_txtMessage")).sendKeys(Case2);
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Arrow")).click();
    driver.findElement(By.xpath("//li[18]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).sendKeys("Corporate QA9 (svuqacorporate_9)");
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).sendKeys("Corporate QA9 (svuqacorporate_9)");
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_RdShareUsers_Input")).sendKeys("Corporate QA9 (svuqacorporate_9)");
    driver.findElement(By.id("ctl00_MainContent_btnsaveShare")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if ("Report has been shared successfully".equals(closeAlertAndGetItsText())) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.id("ctl00_imgLogout")).click();
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("Svuqacorporate_9");
    // ERROR: Caught exception [unknown command [type	]]
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.linkText("Maintain Reports")).click();
    driver.findElement(By.linkText("Saved On")).click();
    driver.findElement(By.linkText("Saved On")).click();
    assertEquals(Case1, driver.findElement(By.id("ctl00_MainContent_grdMembers_ctl00_ctl04_lblNewsTitle")).getText());
    assertEquals("Business Review Dashboard - SUPERVALU Hierarchy", driver.findElement(By.id("ctl00_MainContent_grdMembers_ctl00_ctl04_lblReportName")).getText());
    // ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
    assertEquals("Message from  TestUser Corporate (svuqacorporate):", driver.findElement(By.cssSelector("div.ShareHeader")).getText());
    assertEquals(Case2, driver.findElement(By.id("divInnerMessage")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
