package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ExportFilters {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testExportFilters() throws Exception {
    // ERROR: Caught exception [ERROR: Unsupported command [setSpeed | 3000 | ]]
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    driver.findElement(By.linkText("SUPERVALU Hierarchy")).click();
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).clear();
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).sendKeys("325266");
    driver.findElement(By.id("ctl00_MainContent_VendorApply_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB, FARM FRESH");
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    driver.findElement(By.xpath("//li[4]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("CUB DIAMOND LAKE LLC");
    driver.findElement(By.id("ctl00_MainContent_btnapplysubbanner_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Arrow")).click();
    driver.findElement(By.cssSelector("input.rcbCheckBox")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("");
    driver.findElement(By.xpath("//li[2]/label/input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("AUTHENTIC HISPANIC");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("AUTHENTIC HISPANIC");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("AUTHENTIC HISPANIC");
    driver.findElement(By.id("ctl00_MainContent_btnApplyCategory_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).sendKeys("WAREHOUSE SNACKS");
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).sendKeys("WAREHOUSE SNACKS");
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubCategories_Input")).sendKeys("WAREHOUSE SNACKS");
    driver.findElement(By.id("ctl00_MainContent_btnApplySubCategory_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSalesBy_Arrow")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSalesBy_Arrow")).click();
    driver.findElement(By.xpath("//li[2]")).click();
    driver.findElement(By.id("ctl00_MainContent_btnapplySalesBy_input")).click();
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    driver.findElement(By.id("ctl00_MainContent_btnsaveRepstatus")).click();
    driver.findElement(By.id("ctl00_MainContent_btnExport")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | popup_window | 30000]]
    assertEquals("Recent Timeframe:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[3]")).getText());
    assertEquals("Period", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[4]")).getText());
    assertEquals("Business Review Dashboard -", driver.findElement(By.cssSelector("div.textLayer > div")).getText());
    assertEquals("SUPERVALU Hierarchy", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[2]")).getText());
    assertEquals("Smart Text:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[5]")).getText());
    assertEquals("Enabled", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[6]")).getText());
    assertEquals("Vendor:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[7]")).getText());
    assertEquals("325266, GENERAL MILLS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[8]")).getText());
    assertEquals("Banner(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[9]")).getText());
    assertEquals("CUB, FARM FRESH, HORNBACHERS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[10]")).getText());
    assertEquals("Sub Banner(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[11]")).getText());
    assertEquals("CUB DIAMOND LAKE LLC, CUB FOODS, CUB FRANCHISE, FARM", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[12]")).getText());
    assertEquals("FRESH, HORNBACHERS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[13]")).getText());
    assertEquals("Category(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[14]")).getText());
    assertEquals("ALTERNATIVE SALTY SNACKS, AUTHENTIC HISPANIC", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[15]")).getText());
    assertEquals("Sub-Category(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[16]")).getText());
    assertEquals("WAREHOUSE SNACKS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[17]")).getText());
    assertEquals("Segment(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[18]")).getText());
    assertEquals("All", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[19]")).getText());
    assertEquals("Brand Type:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[20]")).getText());
    assertEquals("All", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[21]")).getText());
    assertEquals("Sales By:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[22]")).getText());
    assertEquals("ID Sales", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[23]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
