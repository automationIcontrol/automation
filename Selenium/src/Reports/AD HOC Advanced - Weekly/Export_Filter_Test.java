package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class ExportFilterTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://svinsights.icucsolutions.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }
// Feature: "Export Filters" functionality check
  @Test
  public void testExportFilter() throws Exception {
    // Given "Login page" opened
    driver.get(baseUrl + "/Login.aspx");
    // When I insert the "Username" and "Password"
    // And accept the "Terms & Conditions"
    driver.findElement(By.id("txtusname")).clear();
    driver.findElement(By.id("txtusname")).sendKeys("svuqacorporate");
    driver.findElement(By.id("txtpass")).clear();
    driver.findElement(By.id("txtpass")).sendKeys("svuq!@#");
    driver.findElement(By.id("chkAccept")).click();
    driver.findElement(By.id("ImageButton1")).click();
    // Then I logged in as Corporate user
    
    // When I click on "SUPERVALU Hierarchy - Weekly" under "ADHOC Advanced" menu
    // Then "AD HOC Advanced - Weekly" report page opened
    driver.findElement(By.xpath("(//a[contains(text(),'SUPERVALU Hierarchy - Weekly')])[12]")).click();
    
    // When I click on arrow on "Week View" drop-down
    driver.findElement(By.id("ctl00_MainContent_ddlWeekView_Arrow")).click();
    driver.findElement(By.xpath("//li[2]")).click();
    // And I select "Ad Week"
    driver.findElement(By.id("ctl00_MainContent_ddlWeekView_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlWeekView_Input")).sendKeys("AD Week");
    // Then "Ad Week" selected
    
    // When I clear "From" date field
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).clear();
    // And insert the "07/30/2017"
    driver.findElement(By.id("ctl00_MainContent_dtFromDate_txtCalendar")).sendKeys("07/30/2017");
    // And clear "To" date field
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).clear();
    // And insert the "08/19/2017"
    driver.findElement(By.id("ctl00_MainContent_dtToDate_txtCalendar")).sendKeys("08/19/2017");
    // Then date frame applied
    
    // When I clear "Vendor Number" field
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).clear();
    // And insert the "325266"
    driver.findElement(By.id("ctl00_MainContent_txtVendorNumber")).sendKeys("325266");
    // And click on "Apply" button
    driver.findElement(By.id("ctl00_MainContent_VendorApply_input")).click();
    // Then "Vendor number" applied
    // And "Department" drop-down filtered in dependent from applied vendor number 
    
    // When I click on arrow on "Baner" drop-down
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Arrow")).click();
    // And select "Cub" banner
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    // And select "HORNBACHERS" banner
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("2 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("2 items checked");
    // And select "SHOP-N-SAVE" banner
    driver.findElement(By.xpath("//li[5]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("3 items checked");
    // And select "SNS EAST" banner
    driver.findElement(By.xpath("//li[7]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlBanners_Input")).sendKeys("CUB");
    // And click on "Apply" button
    driver.findElement(By.id("ctl00_MainContent_btnApplyBanner_input")).click();
    // Then "CUB, HORNBACHERS, SHOP-N-SAVE, SNS EAST" applied
    
    // When I click on arrow on "Sub-Banner" drop-down
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Arrow")).click();
    // And deselect "CUB DIAMOND LAKE LLC" sub-banner
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("6 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("6 items checked");
    // And deselect "CUB MINORITY OWNED LLCS" sub-banner
    driver.findElement(By.xpath("//li[4]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("5 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlSubBanners_Input")).sendKeys("CUB DIAMOND LAKE LLC");
    // And click on "Apply" button
    driver.findElement(By.id("ctl00_MainContent_btnapplysubbanner_input")).click();
    // Then "CUB FOODS, CUB FRANCHISE, HORNBACHERS, SNS EAST, SNS ST LOUIS" sub-banners applied
    
    
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Arrow")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY");
    driver.findElement(By.xpath("//li[2]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY, DAIRY");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY, DAIRY");
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlDepartments_Input")).sendKeys("BAKERY");
    driver.findElement(By.id("ctl00_MainContent_btnApplyDept_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Arrow")).click();
    driver.findElement(By.xpath("//li[3]/label")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("4 items checked");
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlCategories_Input")).sendKeys("BAKERY SWEET GOODS BREAKFAST");
    driver.findElement(By.id("ctl00_MainContent_btnApplyCategory_input")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Arrow")).click();
    driver.findElement(By.xpath("//li[3]")).click();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Input")).clear();
    driver.findElement(By.id("ctl00_MainContent_ddlbrandtype_Input")).sendKeys("National Brand");
    driver.findElement(By.id("ctl00_MainContent_btnapplybrandtype_input")).click();
    driver.findElement(By.id("ctl00_MainContent_btnShowReport")).click();
    driver.findElement(By.id("ctl00_MainContent_btnExport")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | popup_window | 30000]]
    // ERROR: Caught exception [ERROR: Unsupported command [selectPopUp |  | ]]
    assertEquals("AD Week", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[4]")).getText());
    assertEquals("Week View:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[3]")).getText());
    assertEquals("From Date:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[5]")).getText());
    assertEquals("To Date:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[7]")).getText());
    assertEquals("07/30/2017", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[6]")).getText());
    assertEquals("08/19/2017", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[8]")).getText());
    assertEquals("Vendor:", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[9]")).getText());
    assertEquals("325266, GENERAL MILLS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[10]")).getText());
    assertEquals("Banner(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[11]")).getText());
    assertEquals("CUB, HORNBACHERS, SHOP-N-SAVE, SNS EAST", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[12]")).getText());
    assertEquals("Sub Banner(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[13]")).getText());
    assertEquals("CUB FOODS, CUB FRANCHISE, HORNBACHERS, SNS EAST, SNS ST", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[14]")).getText());
    assertEquals("LOUIS", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[15]")).getText());
    assertEquals("Department(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[16]")).getText());
    assertEquals("BAKERY, DAIRY", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[17]")).getText());
    assertEquals("Category(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[18]")).getText());
    assertEquals("BAKERY SWEET GOODS BREAKFAST, MILK, REFRIGERATED DOUGH,", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[19]")).getText());
    assertEquals("YOGURT", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[20]")).getText());
    assertEquals("Sub-Category(s):", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[21]")).getText());
    assertEquals("National Brand", driver.findElement(By.xpath("//div[@id='viewer']/div/div[2]/div[30]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
